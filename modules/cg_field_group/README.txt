INTRODUCTION
------------

"Content Guide: Field Group" integrates Content Guide with Field Group.


REQUIREMENTS
------------

 * Field Group (https://www.drupal.org/project/field_group)
 * Patch from https://www.drupal.org/project/field_group/issues/2985247


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.
